<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Contact;
use Illuminate\Http\UploadedFile;
use Database\Factories\ContactFactory;

class ContactTest extends TestCase
{

    use WithFaker;
//    use RefreshDatabase;

    /**
     * @test
     */
    public function check_if_contact_number_of_columns_is_correct()
    {
        $contact = new Contact();
        $expected = [
            'name',
            'email',
            'phone',
            'message',
            'attachment',
            'senders_ip',
            'shipping_date',
        ];
        $arrayCompared = count($expected) - count($contact->getFillable());
        $this->assertEquals(0, $arrayCompared);
    }

    /**
     * @test
     */
    public function check_if_contact_columns_is_correct()
    {
        $contact = new Contact();
        $expected = [
            'name',
            'email',
            'phone',
            'message',
            'attachment',
            'senders_ip',
            'shipping_date',
        ];
        $arrayCompared = array_diff($expected, $contact->getFillable());
        $this->assertEquals(0, count($arrayCompared));
    }

    /**
     * @test
     */
    public function check_if_contact_register_is_working()
    {
        $file = UploadedFile::fake()->create(md5(date('now')), 500, 'application/pdf');

        $data = ContactFactory::factoryForModel('Contact')->make([
                    'attachment' => $file,
                ])->toArray();

        $response = $this->post(route('contatos.store'), $data);
        $response->assertCreated();
    }
    
    /**
     * @test
     */
    public function checke_if_displays_all_contacts_saves()
    {
        $response = $this->get(route('contatos.index'));
        $response->assertOk();
    }

}
