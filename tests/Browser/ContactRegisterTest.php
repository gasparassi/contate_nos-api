<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\Models\Contact;
use Illuminate\Http\UploadedFile;

class ContactRegisterTest extends DuskTestCase
{

    use WithFaker;

    /**
     * @test
     */
    public function check_if_root_site_is_correct()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(route('contatos.index'))
                    ->assertSee('id');
        });
    }

}
