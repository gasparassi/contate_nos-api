# Contate_nos-api

API  para consumo do app para envio de contato com a empresa.

# Documentação

Para exeutar o projeto, você deverá ter instalado o docker em seu computador. <br />
Você poderá seguir esse passo a passo para exeutar a instalação: [clique aqui](https://balta.io/blog/docker-instalacao-configuracao-e-primeiros-passos) <br />

Após a instalação do docker, você deverá acessar a pasta do projeto e executar o seguinte comando: <br/>
`docker-compose up -d` <br/>

Você deverá realizar a configuração dos parâmetros necessários para rodar o projeto, que será: <br />
Realizar uma cópia do arquivo .env.example com o comando: <br />
`cp .env.example .env` <br/><br/>
As informações referentes à conexão com banco de dados deverá estar conforme abaixo: <br/>
`DB_CONNECTION=mysql`<br/>
`DB_HOST=172.19.0.100`<br/>
`DB_PORT=3306`<br/>
`DB_DATABASE=laravel`<br/>
`DB_USERNAME=usr_desenv`<br/>
`DB_PASSWORD=usr_desenv`<br/>

Para o envio de e-mail para um endereço previamente cadastrado as configurações do arquivo .env deverá estar <br/>
com as informações do seu servidor de e-mail. Para fins de aprendizado e testes, você poderá criar uma conta<br/>
no [mailtrap](https://mailtrap.io/).<br/><br/>
Após a criação da sua conta no mailtrap, você deverá configurar o laravel para utilizá-la para o envio de emails.<br/>
As configurações deverão estar: <br/>
`MAIL_MAILER=smtp`<br/>
`MAIL_HOST=smtp.mailtrap.io`<br/>
`MAIL_PORT=2525`<br/>
`MAIL_USERNAME=seu_user_name_do_mailtrap`<br/>
`MAIL_PASSWORD=sua_senha_do_mailtrap`<br/>
`MAIL_ENCRYPTION=tls`<br/>
`MAIL_FROM_ADDRESS=null`<br/>
`MAIL_FROM_NAME="${APP_NAME}"`<br/>

Você deverá executar o o comando `php artisan key:generate` <br/>

Após a configuração inicial do app com a subida dos containers docker, você deverá executar o comando abaixo para a criação das tabelas no banco de dados:<br />
`php artisan migrate` <br/>

Para gerar os dados para testes, o comando abaixo deverá ser exeutado: <br/>
`php artisan db:seed` <br/>

Após a inserção dos dados de teste, você poderá visualizar o retorno da api acessando o seguinte endereço no browser: <br/>
`http://localhost:8000/api/v1/contacts`
