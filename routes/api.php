<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::get('/', function () {
    return response()
                    ->json([
                        'message' => 'Contate-nos API',
                        'status' => 'Connected',
                        'statusCode' => 200,
                            ],
                            200);
}
);

Route::group([
//    'middleware' => 'users',
    'prefix' => 'v1',
        ], function () {
    Route::group([
        'prefix' => 'contacts',
            ], function () {
        Route::post('/', [ContactController::class, 'store'])->name('contatos.store');
        Route::get('/', [ContactController::class, 'index'])->name('contatos.index');
    });
});
