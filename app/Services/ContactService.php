<?php

namespace App\Services;

use App\Repositories\Contracts\ContactRepositoryInterface;
use App\Models\Contact;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;

/**
 * Description of ContactService
 *
 * @author eder
 */
class ContactService
{

    protected $contactRepository;

    function __construct(ContactRepositoryInterface $ContactRepository)
    {
        $this->contactRepository = $ContactRepository;
    }

    public function makeNewContact($request)
    {
        $contactModel = new Contact();

        $file = $request->file('attachment');
        
        if ( $file->isValid() ) {
            $contactModel->attachment = $this->saveFile($file);
        }
        
        $contactModel->name = $request->name;
        $contactModel->email = $request->email;
        $contactModel->phone = $request->phone;
        $contactModel->message = $request->message;
        $contactModel->senders_ip = $request->ip();
        $contactModel->shipping_date = date('Y-m-d H:i:s');

        $contactRegistered = $this->contactRepository->createNewContact($contactModel);

        if ( $contactRegistered ) {
            $this->sendMail($contactRegistered->toArray());
        }

        return $contactRegistered;
    }

    private function sendMail($details)
    {
        Mail::to('gasparassi@gmail.com')->send(new ContactMail($details));
    }

    private function saveFile($file)
    {
        $dirFile = "files/contacts/";
        $extensionFile = $file->extension();
        $newNameFile = md5(strtotime("now"));
        $newNameFileCompleted = $newNameFile . "." . $extensionFile;
        $file->move(public_path($dirFile), $newNameFileCompleted);

        return $dirFile . $newNameFileCompleted;
    }

    public function getAllContacts()
    {
        $contacts = $this->contactRepository->getAllContacts();

        if ( count($contacts) > 0 ) {
            return $contacts;
        } else {
            return null;
        }
    }

}
