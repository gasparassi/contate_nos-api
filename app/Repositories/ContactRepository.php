<?php

namespace App\Repositories;

use App\Repositories\Contracts\ContactRepositoryInterface;
use App\Models\Contact;

/**
 * Description of ContactRepository
 *
 * @author eder
 */
class ContactRepository implements ContactRepositoryInterface
{

    private $entity;

    /**
     * Construtor inicial da classe
     * @param Contact $contact
     */
    public function __construct(Contact $contact)
    {
        $this->entity = $contact;
    }

    /**
     * Cria um novo contato no banco de dados
     * @param Contact $contact
     */
    public function createNewContact(Contact $contact)
    {
        return $this->entity->create($contact->toArray());
    }

    /**
     * Recupera todos os contatos do banco de dados
     */
    public function getAllContacts()
    {
        return $this->entity->all();
    }

}
