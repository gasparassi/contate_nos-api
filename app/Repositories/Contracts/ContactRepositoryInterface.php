<?php

namespace App\Repositories\Contracts;

/**
 *
 * @author eder
 */
use App\Models\Contact;

interface ContactRepositoryInterface
{

    /**
     * Construtor inicial da classe
     * @param Contact $contact
     */
    public function __construct(Contact $contact);

    /**
     * Cria um novo contato no banco de dados
     * @param Contact $contact
     */
    public function createNewContact(Contact $contact);

    /**
     * Recupera todos os contatos do banco de dados
     */
    public function getAllContacts();
}
