<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{

    use Queueable,
        SerializesModels;

    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $dirAttachment = $this->details['attachment'];
        return $this
                ->with($this->details)
                ->from($this->details['email'])
                ->attach(public_path("$dirAttachment"))
                ->subject('Novo contato registrado!')
                        ->view('emails.contact');
    }

}
