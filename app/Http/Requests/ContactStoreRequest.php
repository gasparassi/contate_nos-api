<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequestCustom as FormRequest;

class ContactStoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
            'email' => 'required|string|email:rfc,dns|regex:/^.+@.+$/i',
            'phone' => 'required|string|celular_com_ddd',
            'message' => 'required|string|max:255',
            'attachment' => 'required|file|between:1,500|mimes:doc,docx,pdf,odt,txt',
        ];
    }

}
