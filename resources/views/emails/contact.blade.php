<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EsocialBrasil</title>
    </head>
    <body>
        <h1>Novo contato registrado!</h1>
        <hr>
        De: <b>{{ $details['name'] }}</b> <br>
        E-mail: <b>{{ $details['email'] }}</b> <br>
        Telefone: <b>{{ $details['phone'] }}</b> <br>
        Mensagem: <b>{{ $details['message'] }}</b> <br>
        Anexo: <a href="{{ $details['attachment'] }}">Download do arquivo</a> <br>
        IP do remetente: <b>{{ $details['senders_ip'] }}</b> <br>
        Data e hora de envio: <b>{{ $details['shipping_date'] }}</b> <br>
        <hr>
    </body>
</html>
