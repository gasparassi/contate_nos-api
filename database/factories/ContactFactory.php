<?php

namespace Database\Factories;

use App\Models\Contact;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;

class ContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Contact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => $this->faker->cellphoneNumber,
            'message' => $this->faker->text,
            'attachment' => UploadedFile::fake()->create(md5(date('now')), 500, 'application/pdf'),
            'senders_ip' => $this->faker->ipv4,
            'shipping_date' => date('Y-m-d H:i:s'),
        ];
    }
}
